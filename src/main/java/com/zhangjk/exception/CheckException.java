package com.zhangjk.exception;

import com.zhangjk.enums.ResultEnum;

/**
 * Created by zhangjk on 2019/4/21.
 */
public class CheckException extends RuntimeException{
    private Integer code;
    public CheckException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());

        this.code = resultEnum.getCode();
    }

    public CheckException(Integer code, String message) {
        super(message);
        this.code = code;
    }


    public Integer getCode() {
        return code;
    }
}
