package com.zhangjk.exception;

import com.zhangjk.util.ResultVOUtil;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class CheckExceptionHandler {

    @ExceptionHandler(value = CheckException.class)
    @ResponseBody
    public Object bizErrorHandler(HttpServletResponse resp, CheckException e) throws Exception {
        resp.setStatus(HttpStatus.LENGTH_REQUIRED.value());
        return ResultVOUtil.error(e.getCode(), e.getMessage());
    }
}
