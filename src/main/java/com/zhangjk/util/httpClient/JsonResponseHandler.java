/* Copyright(c)2010-2014 CN.COM.TCXY.COM
 * Email:chs.garea@gmail.com
 * 
 */ 
 
package com.zhangjk.util.httpClient;


import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * <p> </p>
 * @author chs.garea
 * @param <T>
 * @date 2014-3-29 下午8:48:41
 */
public class JsonResponseHandler implements ResponseHandler<JSONObject> {

	public JSONObject handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {
		int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
            HttpEntity entity = response.getEntity();
            try {
            	return entity != null ? JSONObject.fromObject(EntityUtils.toString(entity, Consts.UTF_8)) : null;
			} catch (JSONException e) {
				throw new ClientProtocolException("Json format error: " + e.getMessage());
			}
        } else {
            throw new ClientProtocolException("Unexpected response status: " + status);
        }
	}

}
