/* Copyright(c)2010-2014 CN.COM.TCXY.COM
 * Email:chs.garea@gmail.com
 * 
 */ 
 
package com.zhangjk.util.httpClient;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * <p> </p>
 * @author chs.garea
 * @date 2014-3-29 下午9:07:23
 */
public class StringResponseHandler implements ResponseHandler<String> {

	public String handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {
		int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
            HttpEntity entity = response.getEntity();
            return entity != null ? EntityUtils.toString(entity, Consts.UTF_8) : null;
        } else {
            throw new ClientProtocolException("Unexpected response status: " + status);
        }
	}

}
