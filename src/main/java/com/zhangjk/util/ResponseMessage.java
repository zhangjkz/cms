package com.zhangjk.util;

import lombok.Data;

/**
 * @description:
 * @author: qi
 * @create: 2019-05-07 17:32
 **/
@Data
public class ResponseMessage {
    // 是否成功 //0成功 1失败
    private int status;

    // 说明
    private String message;

    // 返回结果
    private Object result;

}
