package com.zhangjk.util;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * JWT Token实体
 *
 * 2018-10-23
 * hsc
 */
@Data
@Accessors(chain = true)
public class JwtToken {

    // 分支机构代码
    private String openid;
    // 分支机构名称
    private String session_key;
    // 过期时间
    private Date expiration;

}
