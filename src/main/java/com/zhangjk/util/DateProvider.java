package com.zhangjk.util;

import java.util.Date;

/**
 * @Author: 齐振江
 * @Description:
 * @Date: Created in 16:39 2018/6/6
 * @Modified By:
 */
public interface DateProvider {

    /**
     * 获取当前时间
     * @return 当前时间
     * @throws Exception
     */
    Date getCurrentTime() throws Exception;

    /**
     * 将Date对象格式化成：yyyy-MM-dd HH:mm:ss
     * @param date Date对象
     * @return 格式化日期字符串
     * @throws Exception
     */
    String formatDatetime(Date date) throws Exception;

    /**
     * 将日期字符串转化为Date对象
     * @param datetime 日期字符串
     * @return date对象
     * @throws Exception
     */
    Date parseDatetime(String datetime) throws Exception;
}
