package com.zhangjk.util.poi;

public class PoiException extends RuntimeException {
    private String exCode;
    private String exInfo;
    private String innerMessage;
    private String stacktrace;

    public String getExCode() {
        return this.exCode;
    }

    public String getExInfo() {
        return this.exInfo;
    }

    public String getInnerMessage() {
        return this.innerMessage;
    }

    public String getStacktrace() {
        return this.stacktrace;
    }

    public PoiException(String code, String info) {
        this(code, info, new Exception());
    }

    public PoiException(String code, String info, Exception ex) {
        this.exCode = code;
        this.exInfo = info;
        this.innerMessage = ex.getMessage();
        StringBuilder stc = new StringBuilder();
        StackTraceElement[] var5 = ex.getStackTrace();
        int var6 = var5.length;

        for(int var7 = 0; var7 < var6; ++var7) {
            StackTraceElement elem = var5[var7];
            stc.append(elem + "\n");
        }

        this.stacktrace = stc.toString();
    }

    public PoiException(String code, String info, String message, String stc) {
        this.exCode = code;
        this.exInfo = info;
        this.innerMessage = message;
        this.stacktrace = stc;
    }


}
