package com.zhangjk.util;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *分页的公共参数
 */
@Data
public class PropertyQuery {

    /**
     * 当前页数
     */
    @ApiModelProperty(value="当前的页数")
    private   Integer PageIndex;

    /**
     * 每页显示的条数
     */
    @ApiModelProperty(value="每页现实的条数")
    private Integer  PageSize;

}
