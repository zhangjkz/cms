package com.zhangjk.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ShztNameUtil {
    public static String getName(String shztCode){
        Map<String,String> map=new HashMap<String,String>();
        map.put("0","待审核");
        map.put("1","审批通过");
        map.put("2","审批不通过");

        Set<String> set = map.keySet();
        for(String zz:set){
            if(zz.equals(shztCode)){
                return map.get(zz);
            }
        }
        return "";
    }
}
