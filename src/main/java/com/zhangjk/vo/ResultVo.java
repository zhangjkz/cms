package com.zhangjk.vo;

import lombok.Data;

/**
 * http请求返回最外层的对象
 * Created by zhangjk on 2018/6/5.
 */
@Data
public class ResultVo<T> {
    /**
     * 错误码.
     */
    private Integer code;
    /**
     * 提示信息.
     */
    private String msg;
    /**
     * 具体内容.
     */
    private T data;



}
