package com.zhangjk.mapper;

import com.zhangjk.pojo.UserOrderDetal;
import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.util.List;

@Mapper
public interface UserOrderDetalMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserOrderDetal record);

    int insertSelective(UserOrderDetal record);

    UserOrderDetal selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserOrderDetal record);

    int updateByPrimaryKey(UserOrderDetal record);

    UserOrderDetal findById(@Param("id") Integer id);

    List<UserOrderDetal> findByOrderid(@Param("orderid") Integer orderid);
}