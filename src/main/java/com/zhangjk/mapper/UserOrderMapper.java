package com.zhangjk.mapper;

import com.zhangjk.pojo.UserOrder;
import com.zhangjk.pojo.userOrderVo.UserOrderFz;import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.util.List;

@Mapper
public interface UserOrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserOrder record);

    int insertSelective(UserOrder record);

    UserOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserOrder record);

    int updateByPrimaryKey(UserOrder record);

    List<UserOrder> findByOrdercode(@Param("ordercode") String ordercode);

    List<UserOrder> findByAll(UserOrder record);

    List<UserOrderFz> findByAllandUser(UserOrderFz record);
}