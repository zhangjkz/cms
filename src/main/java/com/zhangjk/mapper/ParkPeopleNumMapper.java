package com.zhangjk.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.zhangjk.pojo.ParkPeopleNum;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Mybatis Generator 2019/04/27
 */
@Mapper
public interface ParkPeopleNumMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ParkPeopleNum record);

    int insertSelective(ParkPeopleNum record);

    ParkPeopleNum selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ParkPeopleNum record);

    int updateByPrimaryKey(ParkPeopleNum record);

    List<ParkPeopleNum> findByCreateTime(@Param("createTime")String createTime);

    List<ParkPeopleNum> findByAll(ParkPeopleNum record);


}