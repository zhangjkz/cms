package com.zhangjk.mapper;

import com.zhangjk.pojo.Picture;
import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.util.List;

@Mapper
public interface PictureMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Picture record);

    int insertSelective(Picture record);

    Picture selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Picture record);

    int updateByPrimaryKey(Picture record);

    Picture findById(@Param("id") Integer id);

    List<Picture> findByAll(Picture record);

    Picture findAllByCreatecodeAndLx(@Param("createcode") String createcode, @Param("lx") String lx, @Param("state") String state);
}