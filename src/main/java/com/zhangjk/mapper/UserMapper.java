package com.zhangjk.mapper;

import com.zhangjk.pojo.User;
import com.zhangjk.pojo.userVo.UserOrderNum;import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.util.List;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> findByAll(User record);

    List<User> findByPhone(@Param("phone") Long phone);

    User findById(@Param("id") Integer id);

    List<UserOrderNum> findByAllOrderNum(User record);

    List<User> findByCode(@Param("code") String code);

    User findbyopenid(@Param("openid") String openid);

    int updatePhoneByopenid(@Param("updatedPhone") Long updatedPhone, @Param("openid") String openid);

    User login(@Param("code") String code, @Param("password") String password);




}