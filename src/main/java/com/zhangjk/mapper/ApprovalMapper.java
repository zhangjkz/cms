package com.zhangjk.mapper;

import com.zhangjk.pojo.Approval;
import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.util.List;

@Mapper
public interface ApprovalMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Approval record);

    int insertSelective(Approval record);

    Approval selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Approval record);

    int updateByPrimaryKey(Approval record);

    List<Approval> findByAll(Approval record);

    Approval findById(@Param("id") Integer id);

    Approval findMaxYy(@Param("userId") Integer userId);
}