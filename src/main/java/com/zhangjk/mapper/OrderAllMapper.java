package com.zhangjk.mapper;

import com.zhangjk.pojo.OrderAll;
import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface OrderAllMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderAll record);

    int insertSelective(OrderAll record);

    OrderAll selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderAll record);

    int updateByPrimaryKey(OrderAll record);

    List<OrderAll> QueryPageForOrderAll(OrderAll orderAll);

    List<OrderAll> QueryAllByparams(OrderAll orderAll);
}