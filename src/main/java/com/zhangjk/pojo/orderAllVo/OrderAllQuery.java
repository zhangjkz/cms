package com.zhangjk.pojo.orderAllVo;

import com.zhangjk.pojo.OrderAll;
import com.zhangjk.util.PropertyQuery;
import lombok.Data;

@Data
public class OrderAllQuery  extends PropertyQuery {


    private OrderAll orderAll;
}