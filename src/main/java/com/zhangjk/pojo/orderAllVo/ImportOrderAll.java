package com.zhangjk.pojo.orderAllVo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
@Data
public class ImportOrderAll {

    /**
     * 订单号
     */

    private String ordercode;

    /**
     * 订单名称
     */

    private String ordername;

    /**
     * 车牌号
     */

    private String carcode;

    /**
     * 开始地点
     */

    private String startplace;

    /**
     * 结束地点
     */

    private String endplace;

    /**
     * 货物类型
     */

    private String htype;


    /**
     * 货物数量
     */

    private Integer hnum;


    /**
     * 运货时间
     */

   private String sportime;


}
