package com.zhangjk.pojo;

import lombok.Data;

/**
 * Created by Mybatis Generator 2019/04/27
 */
@Data
public class ParkPeopleNum {
    private Integer id;

    private String createTime;

    /**
     * 当天进园区人数
     */
    private Integer peopleInsert;

    /**
     * 当天出园区人数
     */
    private Integer peopleDel;

    public ParkPeopleNum() {
    }

    protected boolean canEqual(Object other) {
        return other instanceof com.zhangjk.pojo.ParkPeopleNum;
    }
}