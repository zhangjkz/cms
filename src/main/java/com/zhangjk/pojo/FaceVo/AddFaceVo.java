package com.zhangjk.pojo.FaceVo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: lucuili
 * Date: 2019-04-25
 * Time: 21:56
 */
@Data
public class AddFaceVo {
    private String faceToken;

    private String logId;

    private String location;

}
