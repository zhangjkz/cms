package com.zhangjk.pojo.FaceVo;

;
import lombok.Data;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: lucuili
 * Date: 2019-04-25
 * Time: 22:18
 */
@Data
public class SearchFaceVo {
    private String faceToken;

    private ArrayList userList;

    private UserFaceVo userFaceVo;

}
