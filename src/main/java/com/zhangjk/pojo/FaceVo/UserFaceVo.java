package com.zhangjk.pojo.FaceVo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: lucuili
 * Date: 2019-04-25
 * Time: 22:19
 */
@Data
public class UserFaceVo {
    private String groupId;

    private String userId;

    private String userInfo;

    private float score;
}
