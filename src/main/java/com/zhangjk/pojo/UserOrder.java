package com.zhangjk.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value = "com.zhangjk.pojo.UserOrder")
@Data
public class UserOrder {
    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号")
    private String ordercode;

    /**
     * 订单名称
     */
    @ApiModelProperty(value = "订单名称")
    private String ordername;

    /**
     * 车牌号
     */
    @ApiModelProperty(value = "车牌号")
    private String carcode;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Integer userid;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createtime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updatetime;

    /**
     * 创建人代码
     */
    @ApiModelProperty(value = "创建人代码")
    private String createcode;

    /**
     * 更新人代码
     */
    @ApiModelProperty(value = "更新人代码")
    private String updatecode;
}