package com.zhangjk.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "com.zhangjk.pojo.OrderAll")
@Data
public class OrderAll {
    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号")
    private String ordercode;

    /**
     * 订单名称
     */
    @ApiModelProperty(value = "订单名称")
    private String ordername;

    /**
     * 车牌号
     */
    @ApiModelProperty(value = "车牌号")
    private String carcode;

    /**
     * 开始地点
     */
    @ApiModelProperty(value = "开始地点")
    private String startplace;

    /**
     * 结束地点
     */
    @ApiModelProperty(value = "结束地点")
    private String endplace;

    /**
     * 货物类型
     */
    @ApiModelProperty(value = "货物类型")
    private String htype;

    /**
     * 货物数量
     */
    @ApiModelProperty(value = "货物数量")
    private Integer hnum;

    /**
     * 运货时间
     */
    @ApiModelProperty(value = "运货时间")
    private String sportime;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private String createtime;
}