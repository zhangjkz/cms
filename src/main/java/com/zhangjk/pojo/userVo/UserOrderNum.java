package com.zhangjk.pojo.userVo;

import com.zhangjk.pojo.User;
import lombok.Data;

@Data
public class UserOrderNum extends User {
    private Integer total;
}
