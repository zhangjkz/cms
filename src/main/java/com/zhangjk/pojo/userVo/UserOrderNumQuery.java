package com.zhangjk.pojo.userVo;

import com.zhangjk.pojo.User;
import com.zhangjk.util.PropertyQuery;
import lombok.Data;

@Data
public class UserOrderNumQuery extends PropertyQuery {
    private User user;

}
