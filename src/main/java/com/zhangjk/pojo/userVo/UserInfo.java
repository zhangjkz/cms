package com.zhangjk.pojo.userVo;

import com.zhangjk.pojo.User;
import lombok.Data;

/**
 * @description:
 * @author: qi
 * @create: 2019-06-14 10:21
 **/
@Data
public class UserInfo  extends User {


    private  String token;

}
