package com.zhangjk.pojo.userVo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="com.zhangjk.pojo.userVo.UserVo")
public class UserVo {
// 登录code
@ApiModelProperty(value="登录code")
private String  code;
// 用户头像地址
@ApiModelProperty(value="用户头像地址")
private String avatarUrl;
// 性别
@ApiModelProperty(value="性别")
private String  gender;
// 用户昵称
@ApiModelProperty(value="用户昵称")
private String  nickName;




}
