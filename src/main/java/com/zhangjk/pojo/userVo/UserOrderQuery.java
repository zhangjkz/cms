package com.zhangjk.pojo.userVo;

import com.zhangjk.pojo.UserOrder;
import com.zhangjk.util.PropertyQuery;
import lombok.Data;

@Data
public class UserOrderQuery  extends PropertyQuery {
    private UserOrder userOrder;

}
