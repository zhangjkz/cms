package com.zhangjk.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value = "com.zhangjk.pojo.Approval")
@Data
public class Approval {
    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;

    /**
     * 公司名
     */
    @ApiModelProperty(value = "公司名")
    private String companyName;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Integer userId;

    /**
     * 0待审核  1审批通过  2审批不通过
     */
    @ApiModelProperty(value = "0待审核  1审批通过  2审批不通过")
    private String state;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**
     * 车牌号
     */
    @ApiModelProperty(value = "车牌号")
    private String carCode;

    /**
     * 更新人编码
     */
    @ApiModelProperty(value = "更新人编码")
    private String updateCode;
}