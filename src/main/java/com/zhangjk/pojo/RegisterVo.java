package com.zhangjk.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="com.zhangjk.pojo.RegisterVo")
public class RegisterVo {
    @ApiModelProperty(value="手机号")
    private String  phone;
    @ApiModelProperty(value="验证码")
    private String code;
}
