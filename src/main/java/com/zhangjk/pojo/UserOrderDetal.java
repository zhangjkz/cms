package com.zhangjk.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "com.zhangjk.pojo.UserOrderDetal")
@Data
public class UserOrderDetal {
    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 用户订单id
     */
    @ApiModelProperty(value = "用户订单id")
    private Integer orderid;

    /**
     * 开始地点
     */
    @ApiModelProperty(value = "开始地点")
    private String startplace;

    /**
     * 结束地点
     */
    @ApiModelProperty(value = "结束地点")
    private String endplace;

    /**
     * 货物类型
     */
    @ApiModelProperty(value = "货物类型")
    private String htype;

    /**
     * 运货时间
     */
    @ApiModelProperty(value = "运货时间")
    private String sportime;

    /**
     * 货物数量
     */
    @ApiModelProperty(value = "货物数量")
    private Integer hnum;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private String createtime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private String updatetime;
}