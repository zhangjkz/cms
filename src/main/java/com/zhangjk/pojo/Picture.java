package com.zhangjk.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value = "com.zhangjk.pojo.Picture")
@Data
public class Picture {
    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 主单id
     */
    @ApiModelProperty(value = "主单id")
    private Integer zdid;

    /**
     * 用户Code
     */
    @ApiModelProperty(value = "用户Code")
    private String createcode;

    /**
     * 图片原名字
     */
    @ApiModelProperty(value = "图片原名字")
    private String imgname;

    /**
     * 图片全路径
     */
    @ApiModelProperty(value = "图片全路径")
    private String path;

    /**
     * 图片类型：1:头像  2:车牌  3:人脸识别
     */
    @ApiModelProperty(value = "图片类型：1:头像  2:车牌  3:人脸识别")
    private String lx;

    /**
     * 模块id:1是预约
     */
    @ApiModelProperty(value = "模块id:1是预约")
    private Integer mkid;

    /**
     * 0未使用  1已使用
     */
    @ApiModelProperty(value = "0未使用  1已使用")
    private Long state;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createtime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updatetime;
}