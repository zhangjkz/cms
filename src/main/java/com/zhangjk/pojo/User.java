package com.zhangjk.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value = "com.zhangjk.pojo.User")
@Data
public class User {
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID")
    private Integer id;

    /**
     * 用户编码
     */
    @ApiModelProperty(value = "用户编码")
    private String code;

    /**
     * 微信OPENID
     */
    @ApiModelProperty(value = "微信OPENID")
    private String openid;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 头像url
     */
    @ApiModelProperty(value = "头像url")
    private String imgUrl;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 标志 0正常 1拒绝
     */
    @ApiModelProperty(value = "标志 0正常 1拒绝")
    private Integer state;

    /**
     * 0为普通用户 1为管理员
     */
    @ApiModelProperty(value = "0为普通用户 1为管理员")
    private String bs;

    /**
     * 删除状态 0正常 1删除
     */
    @ApiModelProperty(value = "删除状态 0正常 1删除")
    private Integer sczt;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createcode;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private String updatecode;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createtime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updatetime;

    private Integer total;

}