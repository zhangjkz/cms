package com.zhangjk.pojo.appervaVo;

import com.zhangjk.pojo.Picture;
import lombok.Data;

import java.util.List;
@Data
public class ApprovalPicVO {
    private ApprovalName approval;
    private List<Picture> picList;
}
