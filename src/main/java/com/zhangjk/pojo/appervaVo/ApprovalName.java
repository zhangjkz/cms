package com.zhangjk.pojo.appervaVo;

import com.zhangjk.pojo.Approval;
import lombok.Data;

@Data
public class ApprovalName extends Approval{
    private String stateName;
}
