package com.zhangjk.pojo.appervaVo;

import com.zhangjk.pojo.Approval;
import lombok.Data;

import java.util.List;
@Data
public class ApprovalVo  {
    private Approval approval;
    private List<Integer>  picId;
}
