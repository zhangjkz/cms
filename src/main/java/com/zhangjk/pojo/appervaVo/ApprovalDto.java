package com.zhangjk.pojo.appervaVo;

import com.zhangjk.pojo.Approval;
import com.zhangjk.util.PropertyQuery;
import lombok.Data;

@Data
public class ApprovalDto  extends PropertyQuery {

  private  Approval  approval;
}
