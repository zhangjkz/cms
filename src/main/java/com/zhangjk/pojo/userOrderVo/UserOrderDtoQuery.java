package com.zhangjk.pojo.userOrderVo;

import com.zhangjk.util.PropertyQuery;
import lombok.Data;

@Data
public class UserOrderDtoQuery extends PropertyQuery {
    private UserOrderFz userOrderDto;

}
