package com.zhangjk.pojo.userOrderVo;

import com.zhangjk.pojo.UserOrder;
import lombok.Data;

@Data
public class UserOrderFz extends UserOrder{
    private String userName;
    private String userPhone;
    private String userCode;
}
