package com.zhangjk.pojo.userOrderVo;

import com.zhangjk.pojo.UserOrder;
import com.zhangjk.pojo.UserOrderDetal;
import lombok.Data;

/**
 * @description:
 * @author: qi
 * @create: 2019-06-13 15:27
 **/
@Data
public class UserOrderVo {

    private UserOrder userOrder;

    private UserOrderDetal userOrderDetal;


}
