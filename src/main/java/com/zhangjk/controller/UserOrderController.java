package com.zhangjk.controller;

import com.zhangjk.pojo.OrderAll;
import com.zhangjk.pojo.userOrderVo.UserOrderVo;
import com.zhangjk.pojo.userVo.UserOrderQuery;
import com.zhangjk.service.UserOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(description = "用户订单管理__(移动端)")
@RequestMapping("/userOrder")
public class UserOrderController {
    @Autowired
    private UserOrderService userOrderService;


    @RequestMapping(value = "/serarchOrAdd",method = RequestMethod.POST)
    @ApiOperation(value = "根据订单号和车牌号录入用户订单信息", notes = "")
    public Object serarchOrAdd(@RequestBody OrderAll orederall, HttpServletRequest request)   {
        return userOrderService.serarchOrAdd(orederall,request);
    }



    //查询该用户下订单表（USER_ORDER）时间倒序排列（分页）
    @ApiOperation(value = "查询该用户下订单表", notes = "")
    @RequestMapping(value = "/findByParam",method = RequestMethod.POST)
    public Object findByParam(@RequestBody UserOrderQuery userOrder){
        return userOrderService.findByParam(userOrder);
    }

    //查询该用户下订单表下详情
    @GetMapping("/findByParamDetail")
    @ApiOperation(value = "查询该用户下订单详情信息", notes = "")
    public Object findByParamDetail(@RequestParam(value="orderId", required = false) Integer orderId){
        return userOrderService.findByParamDetail(orderId);
    }

    @ApiOperation(value = "新增用户订单", notes = "")
    @RequestMapping(value = "SaverUserOrderMessage",method = RequestMethod.POST)
    public Object SaverUserOrderMessage(@RequestBody  UserOrderVo userOrderVo){

return userOrderService.SaverUserOrderMessage(userOrderVo);
    }




}
