package com.zhangjk.controller;

import com.zhangjk.pojo.User;
import com.zhangjk.service.UserService;
import com.zhangjk.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by zhangjk on 2019/4/21.
 */
@RestController
@Api(description = "用户管理__(移动端  )")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    //更改用户相关信息
    @PostMapping("/updateSave")
    @ApiOperation(value = "更改用户相关信息__ 移动端更改用户", notes = "")
    public Object updateSave(@RequestBody User user,HttpServletRequest request)   {
        return userService.UpdateUser(user,request);
    }
    @ApiOperation(value = "用户登录__ PC端用户登录", notes = "")
   @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "手机号", paramType = "query", required = true, dataType = "String"),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "query", required = true, dataType = "String")
    })
    public ResponseMessage login(String code, String   password){
    return  userService.login(code,password);
    }




}
