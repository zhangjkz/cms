package com.zhangjk.controller;

import com.zhangjk.service.PictureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@Api(description = "上传")
@RequestMapping("/upload")
public class UploadImgController {
    @Autowired
    private PictureService pictureService;


    //车牌和人脸识别  bs 1:用户头像  2:车牌  3:人脸识别
    @RequestMapping(value = "/chUpload",method = RequestMethod.POST)
    @ApiOperation(value = "图片的上传", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bs", value = "bs 1:用户头像  2:车牌  3:人脸识别", paramType = "query", required = true, dataType = "String")

    })
    public Object chUpload(@RequestParam("file") MultipartFile file,@RequestParam("bs") String bs,HttpServletRequest request) throws Exception {
        return pictureService.chUpload(file,bs, request);
    }
}
