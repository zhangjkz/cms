package com.zhangjk.controller;

import com.zhangjk.pojo.Approval;
import com.zhangjk.pojo.User;
import com.zhangjk.pojo.appervaVo.ApprovalDto;
import com.zhangjk.pojo.appervaVo.ApprovalVo;
import com.zhangjk.service.ApprovalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(description = "预约")
@RequestMapping("/approval")
public class ApprovalController {
    @Autowired
    private ApprovalService approvalService;

    //新增预约   参数：1主单数据  2上传返回的图片Id
    @PostMapping("/save")
    @ApiOperation(value = "新增预约", notes = "")
    public Object save(@RequestBody Approval approval, HttpServletRequest request) throws Exception {
        return approvalService.save(approval,request);
    }

    //管理员 or 普通用户 预约回显（带分页）
    @ApiOperation(value = "预约信息展示", notes = "")
    @RequestMapping(value = "/searchApproval",method = RequestMethod.POST)
    public Object searchApproval(@RequestBody ApprovalDto approvalVo,  HttpServletRequest request)   {
        return approvalService.searchApproval(approvalVo,request);
    }

    @PostMapping("/updateSate")
    @ApiOperation(value = "预约审批", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "aId", value = "预约单id", paramType = "query", required = true, dataType = "String"),
            @ApiImplicitParam(name = "state", value = "通过 或者不通过状态  0 待审核  1 审核通过  2 审核不通过", paramType = "query", required = true, dataType = "String")

    })
    public Object updateSate( String  aId,String  state) throws Exception {
        return approvalService.updateSate(aId,state);
    }




    @GetMapping("/findById")
    @ApiOperation(value = "根据id查询预约单", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "预约单id", paramType = "query", required = true, dataType = "int"),
    })
   public Object findById( Integer id) throws Exception {
        return approvalService.findById(id);
    }




    @PostMapping("/RemoveApproval")
    @ApiOperation(value = "根据id删除预约单", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "预约单id", paramType = "query", required = true, dataType = "int"),
    })
    public Object RemoveApproval( Integer id) throws Exception {
        return approvalService.RemoveApproval(id);
    }

}
