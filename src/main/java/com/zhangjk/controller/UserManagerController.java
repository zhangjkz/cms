package com.zhangjk.controller;

import com.zhangjk.pojo.User;
import com.zhangjk.pojo.userVo.UserOrderNumQuery;
import com.zhangjk.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(description = "用户管理__(PC端)")
@RequestMapping("/userManager")
public class UserManagerController {
    @Autowired
    private UserService userService;

    @ApiOperation(value = "用户列表---查询用户，姓名，电话，车牌，参与订单次数等等信息", notes = "")
    @RequestMapping(value = "/listuser",method = RequestMethod.POST)
    public Object listuser(@RequestBody UserOrderNumQuery query, HttpServletRequest request)   {
    Object  object=     userService.serarchUser(query,request);

        System.out.println(object.toString());
        return object;
    }

    //2.删除 --用户
    @RequestMapping(value = "/deleteUserById",method = RequestMethod.GET)
    @ApiOperation(value = "删除用户", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "UserId", value = "用户id", paramType = "query", required = true, dataType = "int")
    })
    public Object deleteUserById(@RequestParam("UserId") Integer UserId){
        return userService.deleteUserById(UserId);
    }


    @PostMapping("/updateUser")
    @ApiOperation(value = "修改用户信息", notes = "")
    public Object updateUser(@RequestBody User user, HttpServletRequest request) throws Exception {
        return userService.updateUser(user,request);
    }



    @ApiOperation(value = "根据id获取用户信息", notes = "")
    @RequestMapping(value = "/getUser",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "query", required = true, dataType = "String")
    })
    public Object getUser(String userId){
return userService.getUser(userId);
    }


}
