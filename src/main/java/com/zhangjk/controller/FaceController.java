package com.zhangjk.controller;


import com.zhangjk.mapper.ApprovalMapper;
import com.zhangjk.mapper.ParkPeopleNumMapper;

import com.zhangjk.pojo.ParkPeopleNum;

import com.zhangjk.service.FaceService;
import com.zhangjk.util.ResultVOUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;


/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: lucuili
 * Date: 2019-04-25
 * Time: 21:50
 */
@RestController
@RequestMapping("/face")
public class FaceController {

    @Autowired
    private FaceService faceService;
    @Autowired
    private ApprovalMapper approvalMapper;

    @Autowired
    private ParkPeopleNumMapper parkPeopleNumMapper;


    // 人脸搜索（入园区bs=0,出园区bs=1）
    @GetMapping("/searchFace")
    public Object insertSearchFace(@RequestParam("file") File file,@RequestParam("bs") String bs,HttpServletRequest request) throws IOException {
        //查询该用户下最大id的预约表  判断审核状态
      /*  HttpSession session = request.getSession();
        User user = (User)session.getAttribute(UserEnum.USER.getCode());
        Approval maxYy = approvalMapper.findMaxYy(user.getId());
        if(maxYy==null){
            throw new CheckException(ResultEnum.APPROVAL_NULL);
        }else {
            if(maxYy.getState().equals(ZtEnum.SHZT_TG.getCode())){//审批通过
                return this.faceService.searchFace(file);
            }else if(maxYy.getState().equals(ZtEnum.SHZT_BTG.getCode())){//审核不通过
                throw new CheckException(ResultEnum.APPROVAL_NULL_SHBTG);
            }else if(maxYy.getState().equals(ZtEnum.SHZT_DSH.getCode())){//待审核
                throw new CheckException(ResultEnum.APPROVAL_NULL_WSH);
            }else {
                throw new CheckException(ResultEnum.APPROVAL_NULL_SHEWCZT);
            }
        }*/

        return this.faceService.searchFace(file,bs);
    }

    //查看园区进入人数
    @GetMapping("/searchPeopleNum")
    public Object searchPeopleNum(@RequestBody ParkPeopleNum ppn){
      return   ResultVOUtil.success(parkPeopleNumMapper.findByAll(ppn));
    }



}
