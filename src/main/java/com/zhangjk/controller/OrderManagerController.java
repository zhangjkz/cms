package com.zhangjk.controller;

import com.zhangjk.exception.CheckException;
import com.zhangjk.pojo.OrderAll;
import com.zhangjk.pojo.UserOrder;
import com.zhangjk.pojo.orderAllVo.ImportOrderAll;
import com.zhangjk.pojo.orderAllVo.OrderAllQuery;

import com.zhangjk.pojo.orderAllVo.RemoveOrderAllVo;

import com.zhangjk.pojo.userOrderVo.UserOrderDtoQuery;
import com.zhangjk.service.OrderAllService;
import com.zhangjk.service.UserOrderService;
import com.zhangjk.util.poi.ImportExcelUtils;
import com.zhangjk.util.poi.PoiException;

import com.zhangjk.vo.DeleteOrderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/orderManager")
@Api(description = "订单总表__(PC端 )")
public class OrderManagerController {


    // 订单总表组件
    @Autowired
    private OrderAllService orderAllService;

    // 用户订单组件
    @Autowired
    private UserOrderService userOrderService;



    //导入功能--订单总表
    @PostMapping("/upload")
    @ApiOperation(value = "订单总表的导入", notes = "")
    public Object singleFileUpload(@RequestParam("file") MultipartFile file) throws Exception {
        ImportOrderAll xx=new ImportOrderAll();
        List<ImportOrderAll> importOrderAlls=null;
        try {
            importOrderAlls = (List<ImportOrderAll>) ImportExcelUtils.importData(file, xx);
        }catch (PoiException e){
            throw  new CheckException(2001,e.getExInfo());
        }
        return   orderAllService.addOrderAll(importOrderAlls);
    }

    @ApiOperation(value = "订单总表的分页查询", notes = "")
    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public Object listPage(@RequestBody OrderAllQuery query)   {
        return orderAllService.QueryPageForOrderAllwithPage(query);
    }


    @ApiOperation(value = "订单总表根据id去查询", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "订单id", paramType = "query", required = true, dataType = "int")

    })
    @RequestMapping(value = "/QueryOrderAllByid",method = RequestMethod.POST)
    public Object QueryOrderAllByid(Integer id){

        return  orderAllService.QueryOrderAllByid(id);
    }


    //修改功能--订单总表
    @PostMapping("/updateOrderAll")
    @ApiOperation(value = "订单总表的修改", notes = "")
    public Object updateOrderAll(@RequestBody OrderAll orderAll){
        return orderAllService.updateOrderAll(orderAll);
    }
    //删除功能--订单总表
    @PostMapping("/deleteByIds")
    @ApiOperation(value = "订单总表的删除", notes = "")
    public Object deleteByIds(@RequestBody RemoveOrderAllVo  removeOrderAllVo){
        return  orderAllService.deleteByIds(removeOrderAllVo.getListId());
    }


    //查询功能--用户和订单关联的订单表 分页
    @ApiOperation(value = "用户和订单关联的订单表 分页", notes = "")
    @RequestMapping(value = "serarchUserOrder",method =RequestMethod.POST)
    public Object serarchUserOrder(@RequestBody UserOrderDtoQuery query)   {
        return userOrderService.serarchUserOrder(query);
    }
    //删除功能--用户和订单关联的订单表
    @ApiOperation(value = "删除功能--用户和订单关联的订单表", notes = "")
    @PostMapping("/deleteByUo")
    public Object deleteByUo(@RequestBody DeleteOrderVO deleteOrderVO){
        return userOrderService.deleteByUo(deleteOrderVO.getListId());
    }
    //修改功能
    @ApiOperation(value = "修改接口---用户和订单关联的订单表", notes = "")
    @PostMapping("/updateByUo")
    public Object updateByUo(@RequestBody UserOrder uo){
        return userOrderService.updateByUo(uo);
    }

}
