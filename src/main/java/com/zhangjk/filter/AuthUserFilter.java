package com.zhangjk.filter;

import com.zhangjk.enums.UserEnum;
import com.zhangjk.mapper.UserMapper;
import com.zhangjk.pojo.User;
import com.zhangjk.util.JwtToken;
import com.zhangjk.util.UserInfoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
@Component
public class AuthUserFilter implements Filter {
    @Autowired
    private UserMapper userMapper;

       @Autowired
    private UserInfoUtil userInfoUtil;

    //标示符：表示当前用户未登录(可根据自己项目需要改为json样式)
    String NO_LOGIN = "您还未登录";

    String NO_LOGIN_ERRO = "该用户已被停用";

    //不需要登录就可以访问的路径(比如:注册登录等)
     List<String> includeUrls=asList("/login/sendCode","/login/loginOrRegie","/login/loginOut");


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("解决访问权限-------------------------222222222");
        servletRequest.setCharacterEncoding("UTF-8");
        servletResponse.setCharacterEncoding("UTF-8");

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        JwtToken jwtToken= userInfoUtil.getUserInfo();
        String uri = request.getRequestURI();
        System.out.println("filter url:"+uri);
        //是否需要过滤
        boolean needFilter = isNeedFilter(uri);

        if (!needFilter) { //不需要过滤直接传给下一个过滤器
            filterChain.doFilter(servletRequest, servletResponse);
        } else { //需要过滤器
            // session中包含user对象,则是登录状态
            if(jwtToken!=null){
                // System.out.println("user:"+session.getAttribute("user"));

                User user = userMapper.findbyopenid(jwtToken.getOpenid());
                Integer sczt = user.getSczt();
                if(sczt==1 ){//0正常 1拒绝
                    response.getWriter().write(this.NO_LOGIN_ERRO);
                    return;
                }
                filterChain.doFilter(request, response);
            }else{
                String requestType = request.getHeader("X-Requested-With");
                //判断是否是ajax请求
                if(requestType!=null && "XMLHttpRequest".equals(requestType)){
                    response.getWriter().write(this.NO_LOGIN);
                }else{
                    //重定向到登录页(需要在static文件夹下建立此html文件)
                    response.getWriter().write(this.NO_LOGIN);
                   /* response.sendRedirect(request.getContextPath()+"/user/login.html");*/
                }
                return;
            }
        }

    }

    public boolean isNeedFilter(String uri) {
        for (String includeUrl : includeUrls) {
            if(includeUrl.equals(uri)) {
                return false;


            }
        }
       /* return true;*/
        return false;
    }

    @Override
    public void destroy() {

    }
}
