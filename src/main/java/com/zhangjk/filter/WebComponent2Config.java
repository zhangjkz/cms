package com.zhangjk.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebComponent2Config {
    @Autowired
    private AuthUserFilter sessionFilter;

    @Bean
    public FilterRegistrationBean someFilterRegistration1() {
        //新建过滤器注册类
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setOrder(2);
        // 添加我们写好的过滤器
        registration.setFilter(sessionFilter);
        // 设置过滤器的URL模式
        registration.addUrlPatterns("/*");
        return registration;
    }

    @Bean
    public FilterRegistrationBean someFilterRegistration2() {
        //新建过滤器注册类
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setOrder(1);
        // 添加我们写好的过滤器
        registration.setFilter( new CrossDomainFilter());
        // 设置过滤器的URL模式
        registration.addUrlPatterns("/*");
        return registration;
    }
}
