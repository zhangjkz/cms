package com.zhangjk.enums;

import lombok.Getter;

@Getter
public enum ZtEnum {

    SHZT_DSH("0","待审核"),
    SHZT_TG("1","审批通过"),
    SHZT_BTG("2","审批不通过"),

    ZT_PTYH("0","普通用户"),
    ZT_GLY("1","管理员")

    ;

    private String code;
    private String msg;

    ZtEnum(String code,String msg) {
        this.code = code;
        this.msg = msg;
    }
}
