package com.zhangjk.enums;

import lombok.Getter;

@Getter
public enum UserEnum {

    USER("user"),
    USER_vaiCode("vaiCode"),
    USER_CMS_GROUP_ID("userCmsGroupId"),
    ;
    private String code;

    UserEnum(String code) {
        this.code = code;
    }
}
