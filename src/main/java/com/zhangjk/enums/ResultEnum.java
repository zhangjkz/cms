package com.zhangjk.enums;

import lombok.Getter;

/**
 * Created by zhangjk on 2019/4/21.
 */
@Getter
public enum ResultEnum {
    SUCCESS(0, "成功"),

    PARAM_ERROR_PHONE(1, "请输入正确的手机号码"),

    PARAM_ERROR_VICODE(2, "请输入验证码"),

    PARAM_ERROR_WQX(3, "该用户无权限"),

    PARAM_ERROR_PID(4, "上传图片不能为空"),

    PARAM_ERROR_GS(5, "图片只支持.jpg和.png"),

    PARAM_ERROR_ID(6, "用户ID不能为空"),

    ORDER_ID_NULL(7, "订单ID不能为空"),

    PIC_NULL(8, "证照不能为空"),

    PRODUCT_NOT_EXIST(9, "订单不存在"),

    USER_NOT_EXIST(10,"用户不存在"),

    PIC_ERROR_NULL(11, "人脸图片不能为空"),

    PIC_ERROR_NULL_PATH(12, "人脸图片路径不能为空"),

    APPROVAL_NULL(13,"该用户没有预约"),

    APPROVAL_NULL_SHBTG(14,"管理员审核不通过"),

    APPROVAL_NULL_WSH(15,"管理员未审核"),

    APPROVAL_NULL_SHEWCZT(16,"无此审核状态"),


    ORDER_ALL_ID_NULL(17,"订单ID不能为空"),




    WXPAY_NOTIFY_MONEY_VERIFY_ERROR(21, "微信支付异步通知金额校验不通过"),

    ORDER_CANCEL_SUCCESS(22, "订单取消成功"),

    ORDER_FINISH_SUCCESS(23, "订单完结成功"),

    PRODUCT_STATUS_ERROR(24, "商品状态不正确"),

    LOGIN_FAIL(25, "登录失败, 登录信息不正确"),

    LOGOUT_SUCCESS(26, "登出成功"),

    ERROR_FAIL(27,"未知错误"),

    Order_FAIL(28,"没有该订单的信息"),
    ;

    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
