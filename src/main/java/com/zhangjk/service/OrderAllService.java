package com.zhangjk.service;

import com.github.pagehelper.PageInfo;
import com.zhangjk.pojo.OrderAll;
import com.zhangjk.pojo.orderAllVo.OrderAllQuery;
import com.zhangjk.pojo.orderAllVo.ImportOrderAll;

import java.util.List;

public interface OrderAllService {

    /**
     * 更新订单总表
     * @param orderAll
     * @return
     */

     Object updateOrderAll(OrderAll orderAll);


    /**
     * 删除订单总表
     * @param listId
     * @return
     */
    Object deleteByIds(List<Integer> listId);


    /**
     * 新增导入的订单总表
     * @param orderAll
     * @return
     */
     Object addOrderAll(List<ImportOrderAll> orderAll);


    /**
     * 订单总表分页查询
     * @param query
     * @return
     */
    PageInfo<OrderAll> QueryPageForOrderAllwithPage(OrderAllQuery query);



    Object QueryOrderAllByid(Integer id);

}
