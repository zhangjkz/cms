package com.zhangjk.service;

import com.github.pagehelper.PageInfo;
import com.zhangjk.pojo.User;
import com.zhangjk.pojo.userVo.UserOrderNum;
import com.zhangjk.pojo.userVo.UserOrderNumQuery;
import com.zhangjk.pojo.userVo.UserVo;
import com.zhangjk.util.ResponseMessage;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by zhangjk on 2019/4/21.
 */
public interface UserService {


    ResponseMessage RegisterPhone(String phone,String code,HttpServletRequest request) throws   Exception;



    Object getUser(String userId);

    Object UpdateUser(User usr,HttpServletRequest request);

   PageInfo<UserOrderNum> serarchUser(UserOrderNumQuery usr, HttpServletRequest request);

    Object deleteUserById(Integer UserId);

    Object updateUser(User usr,HttpServletRequest request) throws Exception;

    Object updateState(Integer UserId,String state);




   Boolean SaveUser(@RequestBody UserVo userVo,String openid);

   User findbyopenid(@Param("openid")String openid);





   ResponseMessage login(String code, String password);









}
