package com.zhangjk.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface PictureService {
     Object chUpload(MultipartFile file,String bs,HttpServletRequest request) throws Exception;
}
