package com.zhangjk.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhangjk.enums.ResultEnum;
import com.zhangjk.exception.CheckException;
import com.zhangjk.mapper.OrderAllMapper;
import com.zhangjk.mapper.UserMapper;
import com.zhangjk.mapper.UserOrderDetalMapper;
import com.zhangjk.mapper.UserOrderMapper;
import com.zhangjk.pojo.OrderAll;
import com.zhangjk.pojo.User;
import com.zhangjk.pojo.UserOrder;
import com.zhangjk.pojo.UserOrderDetal;
import com.zhangjk.pojo.userOrderVo.UserOrderDtoQuery;
import com.zhangjk.pojo.userOrderVo.UserOrderFz;
import com.zhangjk.pojo.userOrderVo.UserOrderVo;
import com.zhangjk.pojo.userVo.UserOrderQuery;
import com.zhangjk.service.UserOrderService;
import com.zhangjk.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 */
@Service
public class UserOrderServiceImpl implements UserOrderService {
    @Autowired
    private OrderAllMapper orderAllMapper;
    @Autowired
    private UserOrderMapper userOrderMapper;
    @Autowired
    private UserOrderDetalMapper userOrderDetalMapper;

    @Autowired
    private UserInfoUtil userInfoUtil;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private   DateProvider  dateProvider;
@Transactional
    @Override
    public Object serarchOrAdd(OrderAll orederall, HttpServletRequest request) {

        try {
            // 从token获取用户信息
            JwtToken  jwtToken= userInfoUtil.getUserInfo();
            // 根据token的用户信息 获取当前用户的详细信息
            User user=userMapper.findbyopenid(jwtToken.getOpenid());

            List<OrderAll>   orderAlls=   orderAllMapper.QueryAllByparams(orederall);


            if (orderAlls.size()<=0){

                return ResultVOUtil.error(ResultEnum.Order_FAIL.getCode(),ResultEnum.Order_FAIL.getMessage());
            }

            for (OrderAll  orderAll:orderAlls){
                UserOrder userOrder=new UserOrder();
                userOrder.setUserid(user.getId());
                userOrder.setOrdercode(orderAll.getOrdercode());
                userOrder.setOrdername(orderAll.getOrdername());
                userOrder.setCarcode(orederall.getCarcode());
                userOrder.setCreatecode(user.getCode());
                userOrder.setUpdatecode(user.getCode());
                userOrder.setCreatetime(dateProvider.getCurrentTime());
                userOrder.setUpdatetime(dateProvider.getCurrentTime());

                userOrderMapper.insert(userOrder);
                UserOrderDetal record=new UserOrderDetal();
                record.setOrderid(userOrder.getId());
                record.setStartplace(orderAll.getStartplace());
                record.setEndplace(orderAll.getEndplace());
                record.setHtype(orderAll.getHtype());
                record.setHnum(orderAll.getHnum());
                record.setSportime(orderAll.getSportime());

                record.setCreatetime(dateProvider.getCurrentTime().toString());
                record.setUpdatetime(dateProvider.getCurrentTime().toString());

                userOrderDetalMapper.insert(record);

            }
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ResultVOUtil.error(ResultEnum.ERROR_FAIL.getCode(),ResultEnum.ERROR_FAIL.getMessage());
        }

        return ResultVOUtil.success();
    }



    public PageInfo<UserOrder> findByParam(UserOrderQuery query) {
        PageHelper.startPage(query.getPageIndex(), query.getPageSize());
        JwtToken  jwtToken= userInfoUtil.getUserInfo();
        User user=userMapper.findbyopenid(jwtToken.getOpenid());

        if (query.getUserOrder()==null){
            UserOrder userOrder=new UserOrder();
            query.setUserOrder(userOrder);
        }
        query.getUserOrder().setUserid(user.getId());
        return new PageInfo<>(userOrderMapper.findByAll( query.getUserOrder()));
    }



@Transactional
    @Override
public Object SaverUserOrderMessage(UserOrderVo userOrderVo) {
try {
    JwtToken  jwtToken= userInfoUtil.getUserInfo();
    User user=userMapper.findbyopenid(jwtToken.getOpenid());
    UserOrder userOrder=  userOrderVo.getUserOrder();
    userOrder.setUserid(user.getId());
    userOrder.setCreatetime(dateProvider.getCurrentTime());
    userOrder.setCreatecode(user.getCode());
    userOrder.setUpdatetime(dateProvider.getCurrentTime());
    userOrder.setUpdatecode(user.getCode());
    userOrderMapper.insert(userOrder);
    UserOrderDetal userOrderDetal  = userOrderVo.getUserOrderDetal();
    userOrderDetal.setCreatetime(dateProvider.getCurrentTime().toString());
    userOrderDetal.setUpdatetime(dateProvider.getCurrentTime().toString());
    userOrderDetal.setOrderid(userOrder.getId());
    userOrderDetalMapper.insert(userOrderDetal);

}catch (Exception e){
    e.printStackTrace();
    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    return ResultVOUtil.error(ResultEnum.ERROR_FAIL.getCode(),ResultEnum.ERROR_FAIL.getMessage());
}

        return ResultVOUtil.success();
    }

    /**
     *  查询该用户下订单表下详情
     * @param orderId
     * @return
     */
    @Override
    public Object findByParamDetail(Integer orderId) {
        if(orderId==null || orderId==0){
            throw new CheckException(ResultEnum.ORDER_ID_NULL);
        }
        return ResultVOUtil.success(userOrderDetalMapper.findByOrderid(orderId));
    }



    public PageInfo<UserOrderFz> serarchUserOrder(UserOrderDtoQuery query) {
        PageHelper.startPage(query.getPageIndex(), query.getPageSize());
        return new PageInfo<>(userOrderMapper.findByAllandUser(query.getUserOrderDto()));
    }



    @Override
    public Object deleteByUo(List<Integer> listId) {
        if(listId!=null && listId.size()>0){
            listId.stream().forEach((e)->{
                userOrderMapper.deleteByPrimaryKey(e);
            });
            return ResultVOUtil.success();
        }else {
            return ResultVOUtil.error(1006,"删除失败，Id不能为空");
        }

    }
    @Override
    public Object updateByUo(UserOrder uo) {
        if(uo.getId()==null || uo.getId()==0){
            throw new CheckException(ResultEnum.ORDER_ALL_ID_NULL);
        }
        userOrderMapper.updateByPrimaryKeySelective(uo);
        return ResultVOUtil.success();
    }



}
