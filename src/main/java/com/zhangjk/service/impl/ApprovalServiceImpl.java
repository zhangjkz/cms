package com.zhangjk.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhangjk.enums.ResultEnum;
import com.zhangjk.enums.UserEnum;
import com.zhangjk.enums.ZtEnum;
import com.zhangjk.exception.CheckException;
import com.zhangjk.mapper.ApprovalMapper;
import com.zhangjk.mapper.PictureMapper;
import com.zhangjk.mapper.UserMapper;
import com.zhangjk.pojo.Approval;
import com.zhangjk.pojo.Picture;
import com.zhangjk.pojo.User;
import com.zhangjk.pojo.UserOrder;
import com.zhangjk.pojo.appervaVo.ApprovalDto;
import com.zhangjk.pojo.appervaVo.ApprovalName;
import com.zhangjk.pojo.appervaVo.ApprovalPicVO;
import com.zhangjk.pojo.appervaVo.ApprovalVo;
import com.zhangjk.pojo.userVo.userDto;
import com.zhangjk.service.ApprovalService;
import com.zhangjk.service.FaceService;
import com.zhangjk.util.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ApprovalServiceImpl implements ApprovalService {
    @Autowired
    private ApprovalMapper approvalMapper;
    @Autowired
    private PictureMapper pictureMapper;
    @Autowired
    private FaceService faceService;

    @Autowired
    private   UserInfoUtil  userInfoUtil;
    @Autowired
    private UserMapper userMapper;


    @Autowired
    private   DateProvider  dateProvider;


    @Override
    public Object save(Approval approval,HttpServletRequest request) throws Exception {

        JwtToken jwtToken= userInfoUtil.getUserInfo();
        User user=userMapper.findbyopenid(jwtToken.getOpenid());

        approval.setState(ZtEnum.SHZT_DSH.getCode());

        approval.setCreateTime(dateProvider.getCurrentTime());
        approval.setUpdateTime(dateProvider.getCurrentTime());
        approval.setUserId(user.getId());
        approval.setUpdateCode(user.getCode());
        approvalMapper.insertSelective(approval);
        return ResultVOUtil.success();
    }

    @Override
    public Object searchApproval(ApprovalDto approvaldto,HttpServletRequest request) {

        JwtToken jwtToken= userInfoUtil.getUserInfo();
        User user=userMapper.findbyopenid(jwtToken.getOpenid());

        Approval approval1 =approvaldto.getApproval();

        if(user!=null && user.getBs()!=null && user.getBs().equals(ZtEnum.ZT_PTYH.getCode())){//普通用户
            approval1.setUserId(user.getId());
        }//不满足就是管理员权限 查询所有

        PageHelper.startPage(approvaldto.getPageIndex(), approvaldto.getPageSize());
        return new PageInfo<>(approvalMapper.findByAll(approval1));

    }

    @Override
    @Transactional
    public Object updateSate(String aId, String state) throws Exception {

        JwtToken jwtToken= userInfoUtil.getUserInfo();
        User user=userMapper.findbyopenid(jwtToken.getOpenid());
        Approval appz=new Approval();
        appz.setId(Integer.parseInt(aId));
        appz.setState(state);

        appz.setUpdateTime(dateProvider.getCurrentTime());

        appz.setUpdateCode(user.getCode());
        approvalMapper.updateByPrimaryKeySelective(appz);

        return ResultVOUtil.success();
    }

    @Override
    public Object findById(Integer id) {

        Approval  approval=  approvalMapper.findById(id);
        return   ResultVOUtil.success(approval);
    }

    @Override
    public Object RemoveApproval(Integer id) {

        Approval  approval=  approvalMapper.findById(id);

        if (approval!=null){

            if (approval.getState().equals("1")){

                return ResultVOUtil.error(403,"审核通过的预约但不鞥删除");

            }else {
                approvalMapper.deleteByPrimaryKey(id);

                return ResultVOUtil.success();
            }


        }
        return ResultVOUtil.success();
    }
}
