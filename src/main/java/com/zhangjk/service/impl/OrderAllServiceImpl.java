package com.zhangjk.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhangjk.enums.ResultEnum;
import com.zhangjk.exception.CheckException;
import com.zhangjk.mapper.OrderAllMapper;
import com.zhangjk.pojo.OrderAll;
import com.zhangjk.pojo.User;
import com.zhangjk.pojo.orderAllVo.OrderAllQuery;
import com.zhangjk.pojo.orderAllVo.ImportOrderAll;
import com.zhangjk.service.OrderAllService;
import com.zhangjk.service.UserService;
import com.zhangjk.util.DateProvider;
import com.zhangjk.util.JwtToken;
import com.zhangjk.util.ResultVOUtil;
import com.zhangjk.util.UserInfoUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;

@Service
public class OrderAllServiceImpl implements OrderAllService {
    @Autowired
    private OrderAllMapper orderAllMapper;


@Autowired
private DateProvider  dateProvider;


    @Autowired
    private UserInfoUtil userInfoUtil;

    @Autowired
    private UserService  userService;
    /**
     * 订单总表分页查询
     * @param query
     * @return
     */
    public PageInfo<OrderAll> QueryPageForOrderAllwithPage(OrderAllQuery query) {
        PageHelper.startPage(query.getPageIndex(), query.getPageSize());
        return new PageInfo<>(orderAllMapper.QueryPageForOrderAll(query.getOrderAll()));
    }


    /**
     * 根据id去查询订单总表某条记录
     * @param id
     * @return
     */
    @Override
    public Object QueryOrderAllByid(Integer id) {
        return orderAllMapper.selectByPrimaryKey(id);
    }


    /**
     * 修改订单总表
     * @param orderAll
     * @return
     */
    @Override
    public Object updateOrderAll(OrderAll orderAll) {
        if(orderAll.getId()==null || orderAll.getId()==0){
            throw new CheckException(ResultEnum.ORDER_ALL_ID_NULL);
        }
        orderAllMapper.updateByPrimaryKey(orderAll);
        return ResultVOUtil.success();
    }


    /**
     * 删除订单总表
     * @param listId
     * @return
     */
    @Override
    public Object deleteByIds(List<Integer> listId) {
        if(listId!=null && listId.size()>0){
            listId.stream().forEach((e)->{
                orderAllMapper.deleteByPrimaryKey(e);
            });
            return ResultVOUtil.success();
        }else {
            return ResultVOUtil.error(1005,"删除失败，Id不能为空");
        }
    }


    /**
     * 新增导入的订单总表
     * @param orderAlls
     * @return
     */
    @Override
    @Transactional
    public Object addOrderAll(List<ImportOrderAll> orderAlls) {

try {

    for (ImportOrderAll  importOrderAll: orderAlls){
        OrderAll orderAll=new OrderAll();
        BeanUtils.copyProperties(importOrderAll,orderAll);
        orderAll.setCreatetime(dateProvider.getCurrentTime().toString());
        orderAllMapper.insert(orderAll);
    }

}catch (Exception e){
    e.printStackTrace();
    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    return ResultVOUtil.error(ResultEnum.PRODUCT_NOT_EXIST.getCode(),ResultEnum.PRODUCT_NOT_EXIST.getMessage());
}

return ResultVOUtil.success();
    }



}
