package com.zhangjk.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zhangjk.enums.ResultEnum;
import com.zhangjk.enums.UserEnum;
import com.zhangjk.mapper.ParkPeopleNumMapper;
import com.zhangjk.pojo.FaceVo.AddFaceVo;
import com.zhangjk.pojo.FaceVo.SearchFaceVo;
import com.zhangjk.pojo.ParkPeopleNum;
import com.zhangjk.service.FaceService;
import com.zhangjk.util.FaceIdentifyUtil;
import com.zhangjk.util.ResultVOUtil;
import com.zhangjk.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: lucuili
 * Date: 2019-04-25
 * Time: 22:02
 */
@Service
public class FaceServiceImpl implements FaceService {
    @Autowired
    private ParkPeopleNumMapper parkPeopleNumMapper;
    @Override
    public AddFaceVo addFace(String url, String userId, String groupId, String userInfo) {
        File file = new File(url);
        String result = FaceIdentifyUtil.addUser(file, userInfo, userId, groupId);
        return JSONObject.parseObject(result, AddFaceVo.class);
    }

    @Override
    public ResultVo searchFace(File file,String bs) {
        String result = FaceIdentifyUtil.searchFace(file, UserEnum.USER_CMS_GROUP_ID.getCode(), null);
        SearchFaceVo searchFaceVo = JSONObject.parseObject(result, SearchFaceVo.class);

        if (searchFaceVo.getUserFaceVo()== null && searchFaceVo.getUserFaceVo().getScore() > 80){
            // TODO 保存记录入园记录
            if(bs.equals("0")){
                List<ParkPeopleNum> byCreateTime = parkPeopleNumMapper.findByCreateTime(getDate());
                if(byCreateTime!=null && byCreateTime.size()>0){//说明有今天已有人进入
                    ParkPeopleNum parkPeopleNum = byCreateTime.get(0);
                    parkPeopleNum.setPeopleInsert(parkPeopleNum.getPeopleInsert()+1);
                    parkPeopleNumMapper.updateByPrimaryKey(parkPeopleNum);//该天加1
                }else{
                    ParkPeopleNum ppn=new ParkPeopleNum();
                    ppn.setCreateTime(getDate());
                    ppn.setPeopleInsert(1);
                    ppn.setPeopleDel(0);
                    parkPeopleNumMapper.insert(ppn);
                }
            }

            if(bs.equals("1")){//出去
                List<ParkPeopleNum> byCreateTime = parkPeopleNumMapper.findByCreateTime(getDate());
                if(byCreateTime!=null && byCreateTime.size()>0){//说明有今天已有人进入
                    ParkPeopleNum parkPeopleNum = byCreateTime.get(0);
                    parkPeopleNum.setPeopleDel(parkPeopleNum.getPeopleDel()+1);
                    parkPeopleNumMapper.updateByPrimaryKey(parkPeopleNum);//该天加1
                }else{
                    ParkPeopleNum ppn=new ParkPeopleNum();
                    ppn.setCreateTime(getDate());
                    ppn.setPeopleInsert(0);
                    ppn.setPeopleDel(1);
                    parkPeopleNumMapper.insert(ppn);
                }
            }
            return ResultVOUtil.success();
        }
         return ResultVOUtil.error(ResultEnum.USER_NOT_EXIST.getCode(),ResultEnum.USER_NOT_EXIST.getMessage());

    }

    private String getDate(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date());
    }
}
