package com.zhangjk.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zhangjk.enums.ResultEnum;
import com.zhangjk.exception.CheckException;
import com.zhangjk.mapper.PictureMapper;
import com.zhangjk.pojo.Picture;
import com.zhangjk.pojo.User;
import com.zhangjk.pojo.userVo.userDto;
import com.zhangjk.service.PictureService;
import com.zhangjk.service.UserService;
import com.zhangjk.util.DateProvider;
import com.zhangjk.util.JwtToken;
import com.zhangjk.util.ResultVOUtil;
import com.zhangjk.util.UserInfoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Service
public class PictureServiceImpl implements PictureService {
    @Autowired
    private PictureMapper pictureMapper;

    @Autowired
    private UserService userService;


    @Autowired
    private UserInfoUtil userInfoUtil;
    @Autowired
    private DateProvider dateProvider;

@Transactional
    @Override
    public Object chUpload(MultipartFile file, String bs, HttpServletRequest request) throws Exception {


        JwtToken jwtToken=  userInfoUtil.getUserInfo();


        User  user=  userService.findbyopenid(jwtToken.getOpenid());
        // 设置图片上传路径
        File directory = new File("");// 参数为空
        String courseFile = directory.getCanonicalPath();
        System.out.println(courseFile+"img");
        File ffsf = new File(courseFile+"img");
        if(!ffsf.exists()){
            ffsf.mkdirs();
        }
        if(file.getOriginalFilename()==null||file.getOriginalFilename().equals("")){
            throw new CheckException(ResultEnum.PARAM_ERROR_PID);
        }else{
            String picName = UUID.randomUUID().toString();
            // 获取文件名
            String oriName = file.getOriginalFilename();
            // 获取图片后缀
            String extName = oriName.substring(oriName.lastIndexOf("."));
            System.err.println("后缀名字"+extName);
            if(extName.equals(".jpg")||extName.equals(".png")){
                String path=ffsf+"/" + picName + extName;
                // 开始上传
                file.transferTo(new File(path));
                Picture picz=new Picture();
                picz.setImgname(picName+extName);
                picz.setPath(ffsf+"/" + picName + extName);
                picz.setState(0l);//0未使用
                picz.setCreatecode(user.getId().toString());
                picz.setCreatetime(dateProvider.getCurrentTime());
                picz.setUpdatetime(dateProvider.getCurrentTime());
                picz.setLx(bs);
                picz.setMkid(1);//预约平台模块
                pictureMapper.insertSelective(picz);
                System.err.println("上传图片完成-----id"+picz.getId());
                return ResultVOUtil.success(picz.getId());
            }else {
                throw new CheckException(ResultEnum.PARAM_ERROR_PID);
            }
        }

    }
}
