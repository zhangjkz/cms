package com.zhangjk.service;

import com.github.pagehelper.PageInfo;
import com.zhangjk.pojo.OrderAll;

import com.zhangjk.pojo.UserOrder;
import com.zhangjk.pojo.userOrderVo.UserOrderDtoQuery;
import com.zhangjk.pojo.userOrderVo.UserOrderFz;
import com.zhangjk.pojo.userOrderVo.UserOrderVo;
import com.zhangjk.pojo.userVo.UserOrderQuery;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserOrderService {

     Object serarchOrAdd(OrderAll orederall, HttpServletRequest request);

     /**
      * 查询该用户下订单表下详情
      * @param orderId
      * @return
      */
     Object findByParamDetail(Integer orderId);

     Object serarchUserOrder(UserOrderDtoQuery query);


     Object deleteByUo(List<Integer> listId);

     Object  updateByUo(UserOrder uo);

     /**
      * 查询当前用户下的所有的订单
      * @param query
      * @return
      */
     PageInfo<UserOrder> findByParam(UserOrderQuery query);

     Object  SaverUserOrderMessage(UserOrderVo userOrderVo);






}
