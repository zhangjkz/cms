package com.zhangjk.service;

import com.zhangjk.pojo.Approval;
import com.zhangjk.pojo.appervaVo.ApprovalDto;
import com.zhangjk.pojo.appervaVo.ApprovalVo;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletRequest;

public interface ApprovalService {

     Object save(Approval approval, HttpServletRequest request) throws Exception;

     Object searchApproval(ApprovalDto approval, HttpServletRequest request);

     Object updateSate(String aId,String state) throws Exception;

     Object findById(@Param("id") Integer id);

     Object RemoveApproval(Integer id);

}
