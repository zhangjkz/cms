package com.zhangjk.service;

import com.zhangjk.pojo.FaceVo.AddFaceVo;
import com.zhangjk.vo.ResultVo;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: lucuili
 * Date: 2019-04-25
 * Time: 21:54
 */
public interface FaceService {

    /**
     * <p>
     * Description: 百度API用户注册
     * </p>
     *
     * @param url      文件路径
     * @param userId   用户唯一标示 必填
     * @param groupId  用户组id 必填
     * @param userInfo 用户备注 非必填
     * @return 用户信息
     */
    AddFaceVo addFace(String url, String userId, String groupId, String userInfo);

    ResultVo searchFace(File file,String bs);
}
